package com.everis.tdm.services;

import com.everis.tdm.model.giru.Cliente;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.everis.tdm.commons.Util.getTemplate;
import static io.restassured.RestAssured.given;

@SuppressWarnings("PMD.AvoidUsingHardCodedIP")
public class AltaTarjeta {

    private static final Logger LOGGER = LoggerFactory.getLogger(AltaTarjeta.class);

    static private final String ALTA_TARJETA_URL = "https://10.11.32.73:7020/ibk/uat/api/credit-card/v1";

    static private final String TEMPLATE_ALTA_TARJETA = "/request/altaTarjeta.json";

    public static Cliente altaTarjeta(Cliente cliente) {
        LOGGER.info("Card Creation started...");
        String reqAltaTarjeta = getTemplate(TEMPLATE_ALTA_TARJETA).replace("{customerId}", "cliente.getCodigoUnico()");

        Response response = given().relaxedHTTPSValidation()
                .contentType(ContentType.JSON)
                .header("X-INT-Service-Id", "SRM")
                .header("X-B3-ParentSpanId", "3c5c5d43ed4e71ea")
                .header("processtraceid", "XT8052-79214307-20208261554")
                .header("X-INT-Branch-Id", "100")
                .header("X-INT-Country-Id", "PE")
                .header("Authorization", "Basic dUJzZUFzaUFBZG06SWJrYWRtMTgr")
                .header("X-INT-Device-Id", "13.10.13.1")
                .header("X-INT-Server-Id", "ASSICLOUD")
                .header("X-B3-SpanId", "6306f59b437928db")
                .header("X-INT-Net-Id", "AV")
                .header("X-IBM-Client-Id", "9e48c834-31ba-4849-82e3-bada86634d22")
                .header("X-B3-TraceId", "3c5c5d43ed4e71ea")
                .header("X-INT-Consumer-Id", "ASI")
                .header("X-INT-Message-Id", "3c5c5d43ed4e71ea")
                .header("X-INT-User-Id", "XT8052")
                .body(reqAltaTarjeta)
                .when()
                .post(ALTA_TARJETA_URL);
        response.then().statusCode(201);
        LOGGER.info("Card created : " + response.body().path("cardId"));
     //   cliente.setTipoTarjeta("01");
     //   cliente.setFechaCaducidadTarjeta(response.body().path("dueDate"));
      //  cliente.setNumeroTarjeta(response.body().path("cardId"));
        return cliente;
    }

}
