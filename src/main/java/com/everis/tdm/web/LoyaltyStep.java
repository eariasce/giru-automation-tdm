package com.everis.tdm.web;

import com.everis.tdm.model.giru.Cliente;

import org.openqa.selenium.interactions.Actions;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.startsWith;

public class LoyaltyStep extends LoyaltyPage {

    LoyaltyPage page;

    public void openLoyalty() {
        page.open();
    }

    public void doLogin(Cliente cliente) {
        textoUsuario.sendKeys("aalcantaram@proveedor.intercorp.com.pe");
        textoPassword.sendKeys("Loyalty2804");
        btnContinuar.click();
    }

    public void verifyLogin() {
        assertThat(textoBienvenida.getText(), startsWith("Bienvenido"));
    }

    public void doMillas(Cliente cliente) throws InterruptedException {
        openLoyalty();
        doLogin(cliente);
        verifyLogin();
        //cliente.setMillasTarjeta(10000);
        menuHome.click();
        menuFidelizacion.click();
        menuMiembros.click();
        comboLista.click();
        comboTexto.click();
        Actions a1 = new Actions(getDriver());
        btnAgregarFiltro.click();
        a1.clickAndHold(downScroll).perform();
        filtroTextoCodigoUnico.click();
        comboHistoricoTC.click();
        comboHistoricoTexto.click();
        Thread.sleep(2000);
        btnAgregarFiltro.click();

        a1.clickAndHold(downScroll).perform();
        filtroTextoCodigoUnico.click();
       // textoCodigoUnico.sendKeys(cliente.getCodigoUnico());
        botonBuscar.click();
        cerrarFiltro.click();
        nombreMiembro.click();
        menuPerfil.click();
        System.out.println("La cantidad de Millas que tienes son: " + puntosMillaPerfil.getText());
        menuTransacciones.click();
        btnCrearTransaccion.click();
        comboTipoTransaccion.click();
        comboOpcionAcumular.click();
        Thread.sleep(2000);
        comboSubTipoTransaccion.click();
        Thread.sleep(2000);
        comboOpcionSubTipoTransaccion.click();
        comboTipoPuntos.click();
        comboTipoPuntosMillas.click();
        Thread.sleep(2000);
       // textoPuntos.sendKeys(String.valueOf(cliente.getMillasTarjeta()));
        a1.clickAndHold(comboConteptoOperacion).perform();
        comboConceptoTexto.click();
        btnProcesar.click();
        Thread.sleep(2000);
        btnProcesarYes.click();

    }

}
