package com.everis.tdm.dao.giru;


import com.everis.tdm.model.giru.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

@Component
public class giruDAO implements giruImpl {

    public static JdbcTemplate jdbc;

    @Autowired
    public giruDAO(DataSource dataSource) {
        jdbc = new JdbcTemplate(dataSource);
    }


    public void sp_insertCliente(String tipodocumento, String nrodocumento, String operador, String telefono) {
        String SQL = "{call insertarCliente(?,?,?,?)}";
        jdbc.update(SQL, tipodocumento,nrodocumento,operador,telefono);
    }
    public void sp_insertUsuario(String Usuario, String Area, String Contrasena, String Rol) {
        String SQL = "{call insertarUsuario(?,?,?,?)}";
        jdbc.update(SQL, Usuario,Contrasena,Rol,Area);
    }
    public void sp_insertTarjeta(String numtarjeta, String cvv, String fecha,String estado,String tipo) {
        String SQL = "{call insertarTarjeta(?,?,?,?,?)}";
        jdbc.update(SQL, numtarjeta,cvv,fecha,estado,tipo);
    }
    public void sp_updateMillas(String codigoUnico, int millasTarjeta) {
        String SQL = "{call updateMillas(?,?)}";
        jdbc.update(SQL, codigoUnico, millasTarjeta);
    }

    @Override
    public List<Cliente> sp_getClientes() {
        String SQL = "{call getClientes}";
        return jdbc.query(SQL,
                new ClienteMapper());
    }

    public List<Usuario> sp_getUsuarioPorRol(String vrol) {
        String SQL = "{call getUsuarioPorRol(?)}";
        return jdbc.query(SQL, new UsuarioMapper(), new Object[]{vrol});
    }

    public List<Tarjeta> sp_getTarjetaPorEstado(String vestado) {
        String SQL = "{call getTarjetaPorEstado(?)}";
        return jdbc.query(SQL, new TarjetaMapper(), new Object[]{vestado});
    }


    public void sp_updateRegistroBenni(String codigoUnico) {
        String SQL = "{call updateRegistroBenni(?)}";
        jdbc.update(SQL, codigoUnico);
    }

    //--------------------------------------------------------------- Renzo
    // llamando directamente al procedimiento almacenado
    //@Override
    public void sp_insertReclamo(String tipologia, String medioRespuesta) {
        String SQL = "{call insertarReclamo(?,?)}";
        jdbc.update(SQL, tipologia, medioRespuesta);
    }

    //@Override
    public List<Reclamo> sp_getReclamos() {
        String SQL = "{call getClientes}";
        return jdbc.query(SQL,
                new ReclamoMapper());
    }

}
