package com.everis.tdm.dao.giru;

import com.everis.tdm.model.giru.Cliente;
import com.everis.tdm.model.giru.Usuario;

import java.util.List;

public interface giruImpl {

    List<Usuario> sp_getUsuarioPorRol(String vrol);

    void sp_insertCliente(String Usuario, String Area, String Contrasena, String Rol);

    void sp_updateMillas(String codigoUnico, int millasTarjeta);

    void sp_updateRegistroBenni(String codigoUnico);

    List<Cliente> sp_getClientes();

}
