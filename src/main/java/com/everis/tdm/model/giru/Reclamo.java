package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class Reclamo {

    private String tipologia;
    private String medioRespuesta;

}
