package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.everis.tdm.security.Decryption.decrypt;

public class UsuarioMapper implements RowMapper<Usuario> {
    @SneakyThrows
    public Usuario mapRow(ResultSet rs, int arg1) throws SQLException {
        Usuario data = new Usuario();
        data.setUsuario(decrypt(rs.getString("usuario")));
        data.setContrasena(decrypt(rs.getString("contrasena")));
        data.setRol(rs.getString("rol"));
        data.setArea(rs.getString("area"));
        return data;
    }
}